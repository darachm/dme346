#!/usr/bin/env bash

shuf tmp/Undetermined_S0_L001_clustered.table | head -n 20 \
    | xargs -I'{}' echo '@{}' \
    > results_picked_20.table

cut -d, -f1 results_picked_20.table > results_picked_20.ids

zcat data/Undetermined_S0_L001_R1_001.fastq.gz | paste - - - - \
    | grep -Ff results_picked_20.ids \
    > inputs_picked_20.1.table

zcat data/Undetermined_S0_L001_R2_001.fastq.gz | paste - - - - \
    | grep -Ff results_picked_20.ids \
    > inputs_picked_20.2.table

join -t, <(mawk '{print $1","$3}' inputs_picked_20.1.table | sort -t, -k1,1) \
        <(mawk '{print $1","$3}' inputs_picked_20.2.table | sort -t, -k1,1) \
    | join -t, - <( sort -t, -k1,1 results_picked_20.table ) \
    > spot_check.csv

