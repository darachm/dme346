# Phony rules tells make to just ignore these fake files
.PHONY: all archive run_pipeline clean lightclean

# This makes the nextflow executable for running
scripts/nextflow: 
	curl -s https://get.nextflow.io | bash
	mv nextflow scripts/nextflow





# This rule calls the actual nextflow pipeline to run
run_pipeline : scripts/nextflow scripts/run_pipeline.nf scripts/run_pipeline.nfconfig
	export NXF_SINGULARITY_CACHEDIR="$${HOME}/.singularity/" && \
		$< run $(word 2,$^) -c $(word 3,$^)  \
			-resume -ansi-log false -with-dag reports/dag.html \
			--reads 'data/*.f*z' \
			-work-dir w/ork
# Unset the singularity cache so we can just use it 
# a local cache, I think
# No ansi-log so we can debug easier (or it'll hide dirs)
# Makes a pretty DAG






# For archiving, this makes a really compressed zip
archive: data.zip
data.zip: data
	zip -9 -r $@ $<

all: 
	@echo "none"

lightclean:
	@echo "do a light clean? if not cancel cntrl D"
	@read DO_IT
	rm -rf scripts/.ipynb_checkpoints
	rm -rf scripts/*.png
	rm -rf scripts/*.pdf
	rm -rf scripts/*.jpeg
	rm -rf scripts/*_cache
	rm -rf scripts/*_files

clean: lightclean
	@echo "do a deeper clean? if not cancel cntrl D"
	@read DO_IT
	rm -rf tmp
	rm -rf .nextflow
	rm -rf w/ork
	rm -rf reports
