This is a pack-up of Darach's dme317 pipeline, popped into this repo as a 
template pipeline for analyzing iSeq2 constructs. I believe that's the locus
architecture in the glabrata strains. Anyways, should be a good and modular
and scalable pipeline to proc, groc, and output the data. QC and all that.

Might try fastp, that seemed cool.

Details about amplicon and how to chop.
This is necessary to pass into the itermae step, so it knows where to
cut up the reads. Note that it's defined as a list, then joined in
later. So first value in there is which read this is doing, then it's
the commandline arguments for itermae.

The Design for the glabrata amplicon, 5' to 3', with Sasha's primers
AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
read1 starts here, 5' to 3'
NNNNNNNNNNNNNNTTAATATGGACTAAAGGAGGCTTTTGTCGACGGATCCGATATCGGTACC
NNNNNAANNNNNTTNNNNNTTNNNNNATAACTTCGTATAATGTATGCTATACGAAGTTATTGC
GCGGTGATCACTTATGGtaccgTTCGTATAAaGTaTcCTATACGAACGGTATGCGCGGTGATCACTTAT
GGTACCGTTCGTATAATGTGTACTATACGAACGGTAANNNNNAANNNNNTTNNNNNTTNNNNN
GGTACCGATATCAGATCTAAGCTTGAATTCGAGNNNNNNNNNNNNNN
read2 is priming back to read the previous line here, revcomp
AGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGCCGTCTTCTGCTTG

read1 patterns
NNNNNNNNNNNNNNTTAATATGGACTAAAGGAGGCTTTTGTCGACGGATCCGATATCGGTACC
NNNNNAANNNNNTTNNNNNTTNNNNNATAACTTCGTATAATGTATGCTATACGAAGTTATTGC

read2 patterns, by echo "" | rev | tr 'ATCG' 'tagc' 
NNNNNNNNNNNNNNCTCGAATTCAAGCTTAGATCTGATATCGGTACC
NNNNNAANNNNNAANNNNNTTNNNNNTTACCGTTCGTATAGTACACATTATACGAACGGTACC

itermae_arguments = 
    [
        [   "1", // which read to join on
            '-o "input > ^(?<umi>[ATCGN]{7,9}?)(?<sample>[ATCGN]{6,6})(TTAATATGGA){e<=1}(?<rest>.*)$" '+
            '-o "rest > (TATCGGTACC){e<=1}(?<barcode>[ATCGN]{5,5}AA[ATCGN]{5,5}TT[ATCGN]{5,5}TT[ATCGN]{5,5}){e<=3}(ATAACTTCGT){e<=1}" '+
            '--filter "statistics.median(sample.letter_annotations[\\"phred_quality\\"])>=30" '+
            '--filter "statistics.median(barcode.letter_annotations[\\"phred_quality\\"])>=30" '+
            '--output-id "input.id" --output-seq "sample" '+
            '--output-id "input.id" --output-seq "barcode" '+
            ''],
        [   "2",
            '-o "input > ^(?<umi>[ATCGN]{7,9}?)(?<sample>[ATCGN]{9,9})(TCGAATTCAA){e<=1}(?<rest>.*)$" '+
            '-o "rest > (TATCGGTACC){e<=1}(?<barcode>[ATCGN]{5,5}AA[ATCGN]{5,5}AA[ATCGN]{5,5}TT[ATCGN]{5,5}){e<=3}(TTACCGTTCG){e<=1}" '+
            '--filter "statistics.median(sample.letter_annotations[\\"phred_quality\\"])>=30" '+
            '--filter "statistics.median(barcode.letter_annotations[\\"phred_quality\\"])>=30" '+
            '--output-id "input.id" --output-seq "sample" '+
            '--output-id "input.id" --output-seq "barcode" '+
            '']
        ]

With Katja's primer it's like so
AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
read1 starts here, 5' to 3'
0-6 bases of index, then
TTAATATGGACTAAAGGAGGCTTTTGTCGACGGATCCGATATCGGTACC
NNNNNAANNNNNTTNNNNNTTNNNNNATAACTTCGTATAATGTATGCTATACGAAGTTATTGC
GCGGTGATCACTTATGGtaccgTTCGTATAAaGTaTcCTATACGAACGGTATGCGCGGTGATCACTTAT
GGTACCGTTCGTATAATGTGTACTATACGAACGGTAANNNNNAANNNNNTTNNNNNTTNNNNN
GGTACCGATATCAGATCTAAGCTTGAATTCGA
0-6 bases of index, then
AGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGCCGTCTTCTGCTTG

read1 patterns
[ATCGN]+
TTAATATGGACTAAAGGAGGCTTTTGTCGACGGATCCGATATCGGTACC
NNNNNAANNNNNTTNNNNNTTNNNNN
ATAACTTCGTATAATGTATGCTATACGAAGTTATTGC

read2 patterns, by echo "" | rev | tr 'ATCG' 'tagc' 
[ATCGN]+
TCGAATTCAAGCTTAGATCTGATATCGGTACC
NNNNNAANNNNNAANNNNNTTNNNNN
TTACCGTTCGTATAGTACACATTATACGAACGGTACC
        ){e<=2}(?P<barcode>[ATCGN]{22,30})(?P<downPrime>TTACCGTTCGTATAGT){e<=2}" '+

