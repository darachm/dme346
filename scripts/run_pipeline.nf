#!/usr/bin/env nextflow
nextflow.enable.dsl=2
// Above line specifies that we're using the DSL2 version, leave it alone!

// 
// 
// Introduction and preliminaries
// 
// 

// This is a pipeline for analyzing the barcodes for the glabrata library.
// The general idea is to generate some QC reports on the files, then
//   chop out barcodes from reads, cluster those. 
// Analysis is handled with a separate R file, for now.

// For running, consult the Makefile. Basically that runs this Nextflow
// pipeline with an argument that specifies the reads.

//
//
// Organization - this begins with a definition of the 'workflow'. That's the
//   main function of what is done. Note that it's a bunch of 'processes' strung
//   together with pipes. Some of these are custom 'processes' defined after
//   the workflow, some are Nextflow specific functions for changing the 
//   channels of files and information as they flow between steps. Pipe 
//   something into the 'view' process and re-run to see what it is.
//
//

workflow {

    // this is reading in the filenames, splitting them to IDs / read
    Channel.fromPath(params.reads) 
        | map{ (file, run, lane, read) = (it =~ /([^\/]+)_(L\d+)_[Rr]?(\d+)_.+\.f.*z/)[0]
            [ read, run+'_'+lane, it] } 
        | set{ input_fastqz } // This is one way to define a variable!

    // QC with fastqc, multiqc, fastp
        // just fastqc and multiqc
    input_fastqz | fastqc | collect | multiqc
        // arranging as r1 r2 pairs for fastp paired analysis (long one!)
    input_fastqz.groupTuple(by: [1]) | map{ [it[1]] + it[2].sort() } | fastp

    // Cutting up the fastqz with itermae
    input_fastqz 
        | combine(Channel.fromList(itermae_arguments),by: 0) // adding arguments
        | itermae // running, makes SAM file with multiple codes per read
        | split_out_codes // splits these multiple codes
        | transpose // stretches channel to handle one code at a time
        | map{ it -> [] + ( it[2] =~ /.*\/(.*).codes/ )[0][1] + it } // get name
        | combine( // add clustering parameters, based on what kind of code it is
                Channel.of( 
                    ['XI:0','starcode','-d 1 -s '], // for sample barcodes
                    ['XI:1','bartender','-c 1 -z 5 -d 2 -l 3'] // for lineage barcodes
                ),
                by: [0] 
            ) // this combines based on code ID, eg 'XI:1' 
        | branch{ // then we fork it into two channels based on which tool used
                for_starcode: it[5] == 'starcode'
                for_bartender: it[5] == 'bartender'
            } 
        | set{ extracted_codes }

    // do starcode on the sample indicies
    extracted_codes.for_starcode | starcode | set{ starcoded }

    // do bartender on the lineage barcodes
    extracted_codes.for_bartender | bartender | unfold_bartender | set{ bartended }

    // mix together files, merge by run ID, and tabulate counts
    starcoded.mix(bartended) | groupTuple(by: [0]) \
        | remerge_codes | tabulate_barcode_combos
        | view 

    // Ugly attempt at some # records tracking through pipeline:
    input_fastqz | map{ [ it[1]+'_'+it[0], it[2] ] } | records_fastqz | set{ fastqz_records }
    itermae.out | map{ [ it[1]+'_'+it[0], it[2] ] } | records_itermae | set{ itermae_records }
    remerge_codes.out | records_mergecodes | set{ mergecodes_records }
    fastqz_records.concat(itermae_records,mergecodes_records)
        | collect | map{ [ it, file('scripts/sankey_amounts.R') ]} 
        | sankey_records 

}

// Defining the custom processes

/* QC */
process fastqc {
    cpus 1
    memory '8 GB'
    label 'qc'
    input: tuple val(read), val(id), path(fastqz)
    output: path("${fastqz}_qc") 
    shell: '''
mkdir !{fastqz}_qc
fastqc --threads !{task.cpus} -o !{fastqz}_qc !{fastqz}
        '''
}

// Compile FASTQC outputs into a report, publish
process multiqc {
    publishDir 'output'
    cpus 1
    memory '2 GB'
    label 'qc'
    input: path("*")
    output: path("*")
    shell: '''
multiqc ./
        '''
}

// running fastp, which is sort of an alternative to fastqc with more deets
process fastp {
    publishDir 'output'
    cpus 1
    memory '8 GB'
    label 'fastp'
    input: tuple val(id), path(read1), path(read2)
    output: path("*")
    shell: '''
/opt/fastp --in1 !{read1} --in2 !{read2} --overrepresentation_analysis
        '''
}


// itermae arguments, so the chopping
// Amplicon structure:
// with Katja's primer it's like so
// AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
// read1 starts here, 5' to 3'
// 0-6 bases of index, then
// TTAATATGGACTAAAGGAGGCTTTTGTCGACGGATCCGATATCGGTACC
// NNNNNAANNNNNTTNNNNNTTNNNNNATAACTTCGTATAATGTATGCTATACGAAGTTATTGC
// GCGGTGATCACTTATGGtaccgTTCGTATAAaGTaTcCTATACGAACGGTATGCGCGGTGATCACTTAT
// GGTACCGTTCGTATAATGTGTACTATACGAACGGTAANNNNNAANNNNNTTNNNNNTTNNNNN
// GGTACCGATATCAGATCTAAGCTTGAATTCGA
// 0-6 bases of index, then
// AGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGCCGTCTTCTGCTTG
//
// read1 patterns
// [ATCGN]+
// TTAATATGGACTAAAGGAGGCTTTTGTCGACGGATCCGATATCGGTACC
// NNNNNAANNNNNTTNNNNNTTNNNNN
// ATAACTTCGTATAATGTATGCTATACGAAGTTATTGC
//
// read2 patterns, by echo "" | rev | tr 'ATCG' 'tagc' 
// [ATCGN]+
// TCGAATTCAAGCTTAGATCTGATATCGGTACC
// NNNNNAANNNNNAANNNNNTTNNNNN
// TTACCGTTCGTATAGTACACATTATACGAACGGTACC
//

itermae_arguments = 
    [
        [   "1", // which read to join on
            '-o "input > ^(?P<prefix>[ATCGN]{0,8}(TTAATATGGA){e<=1})(?P<rest>.*)" '+
            '-o "rest  > (?P<upPrime>TATCGGTACC){e<=1}(?P<barcode>[ATCGN]{22,30}?)(?P<downPrime>ATAACTTCGT){e<=1}" '+
            '-o "prefix > ^(?P<sample>[ATCGN]{8,8})" '+
            '--filter "statistics.median(barcode.quality)>=30" '+
            '--output-id "input.id" -oseq "sample" '+
            '--output-id "input.id" -oseq "barcode" '+
            '-of "sam" '+
            ''],
        [   "2",
            '-o "input > ^(?P<prefix>[ATCGN]{0,8}(TCGAATTCAA){e<=1})(?P<rest>.*)" '+
            '-o "rest  > (?P<upPrime>TATCGGTACC){e<=1}(?P<barcode>[ATCGN]{22,30}?)(?P<downPrime>TTACCGTTCG){e<=1}" '+
            '-o "prefix > ^(?P<sample>[ATCGN]{8,8})" '+
            '--filter "statistics.median(barcode.quality)>=30" '+
            '--output-id "input.id" -oseq "sample" '+
            '--output-id "input.id" -oseq "barcode" '+
            '-of "sam" '+
            '']
        ]

// itermae! Requires arguments to be specified outside the fuction, so that's
// done in the object itermae_arguments
process itermae {
    cpus 16
    memory '16 GB'
    label 'itermae'
    input: tuple val(read), val(id), path(input_fastqz), val(arguments)
    output: tuple val(read), val(id), path("${id}_${read}_pass.sam")
    shell: '''
zcat !{input_fastqz} \
    | parallel --pipe -L 10000 \
        'itermae !{arguments} -v ' \
        > !{id}_!{read}_pass.sam \
        2> err
'''
}

// split each tag
process split_out_codes { 
    cpus 1
    memory '08 GB'
    label 'munge'
    input: tuple val(read), val(id), path(sam)
    output: tuple val(read), val(id), path("*.codes"), path("*.ids")
    shell: '''
cat !{sam} | cut -f1,10,12 \
    | mawk -F'\t' '{print($1"\t"$3) > $3".ids"; print($2) > $3".codes"}'
'''
}

// clustering 

// starcode - splitting files by the first SAM tag (expect XI:0 etc)
process starcode { 
    publishDir 'tmp'
    cpus 16
    memory '60 GB'
    label 'starcode'
    input: tuple val(which_code), val(read), val(id), path(codes), path(ids),
            val(which_tool), val(parms)
    output: tuple val(id), val(read), val(which_code), 
        path("${read}_${which_code}_starcode.clustered")
    shell: '''
export SORTPARM="--parallel=!{task.cpus} -T ./ -S 30G"
starcode --threads !{task.cpus} --seq-id -i !{codes} !{parms} \
    | cut -f1,3 \
    | mawk '{ split($2,a,","); for (i in a){ print a[i]"\t"$1;} }' \
    | sort ${SORTPARM} -k1,1 -n | cut -f2 | paste !{ids} - \
    | sed 's/\t/,/g' \
    > !{read}_!{which_code}_starcode.clustered
'''
}

/* bartender for lineage long barcodes */
process bartender {
    cpus 16
    memory '60 GB'
    label 'bartender'
    input: tuple val(which_code), val(read), val(id), path(codes), path(ids),
            val(which_tool), val(parms)
    output: tuple val(id), val(read), val(which_code), 
            path("${id}_${read}_${which_code}_for_bartender"), 
            path("${id}_${read}_${which_code}_barcode.csv"), 
            path("${id}_${read}_${which_code}_cluster.csv")
    shell: '''
paste -d, !{codes} <( sed 's/\t/_/' !{ids}) \
    > !{id}_!{read}_!{which_code}_for_bartender
bartender_single_com -t !{task.cpus} !{parms} \
    -f !{id}_!{read}_!{which_code}_for_bartender \
    -o !{id}_!{read}_!{which_code} 
'''
}

process unfold_bartender {
    cpus 16
    memory '20 GB'
    label 'munge'
    input: tuple val(id), val(read), val(which_code), 
            path(input), path(barcodes), path(clusters)
    output: tuple val(id), val(read), val(which_code),
            path("${read}_${which_code}_bartender.clustered")
    shell: '''
export SORTPARM="--parallel=!{task.cpus} -T ./ -S 20G"
join -t, -1 3 -2 1 \
    <( tail -n+2 !{barcodes} | sort ${SORTPARM} -t, -k3,3 ) \
    <( tail -n+2 !{clusters} | sort ${SORTPARM} -t, -k1,1 ) \
    | cut -d, -f2,4 \
    | sort ${SORTPARM} -t, -k1,1 \
    | join -t, -j 1 - <( sort ${SORTPARM} -t, -k1,1 !{input} ) \
    | mawk -F, '{print $3","$2}' \
    | sed 's/_/,/' \
    > !{read}_!{which_code}_bartender.clustered
'''
}

// remerge these 
process remerge_codes { 
    publishDir 'tmp'
    cpus 1
    memory '16 GB'
    label 'munge'
    input: tuple val(id), val(read), val(which_code), path("*")
    output: tuple val(id), path("${id}_clustered.table")
    shell: '''
export SORTPARM="--parallel=!{task.cpus} -T./ -S16G"
function joinliston1 {
    while (( $# )); do
        if [[ ! -f tmp.table ]]; then
            cut -f1,3 -d, $1 | sort ${SORTPARM} -t, -k1,1 > tmp.table
            shift
        else
            join -j1 -t, tmp.table \
                <(cut -f1,3 -d, $1 | sort ${SORTPARM} -t, -k1,1) \
                > tmp.table2
            mv -f tmp.table2 tmp.table
            shift
        fi
    done
}
export -f joinliston1
ls *clustered | xargs -I'{}' bash -c 'joinliston1 {}'
mv tmp.table !{id}_clustered.table
'''
}

process tabulate_barcode_combos { 
    publishDir 'output'
    cpus 1
    memory '16 GB'
    label 'munge'
    input: tuple val(id), path(table)
    output: tuple val(id), path("${id}_tabulated.table")
    shell: '''
export SORTPARM="--parallel=!{task.cpus} -T ./ -S 16G"
cut -d',' -f2- !{table} \
    | sort ${SORTPARM} | uniq -c \
    > !{id}_tabulated.table
'''
}

// records counting
process records_fastqz {
    cpus 1
    memory '8 GB'
    label 'bioinfmunge'
    input: tuple val(id), path(fastqz)
    output: path("${id}.fastqz_records") 
    shell: '''
zcat !{fastqz} | wc -l | xargs -IX sh -c 'echo "X/4" | bc ' \
    > !{id}.fastqz_records
'''
}
process records_itermae {
    cpus 1
    memory '8 GB'
    label 'itermae'
    input: tuple val(id), path(itermae_sam)
    output: path("${id}.itermae_records") 
    shell: '''
grep "XI:0" !{itermae_sam} | wc -l > !{id}.itermae_records
'''
}
process records_mergecodes {
    cpus 1
    memory '8 GB'
    label 'munge'
    input: tuple val(id), path(codes_table)
    output: path("${id}.mergecodes_records") 
    shell: '''
cat !{codes_table} | wc -l > !{id}.mergecodes_records
'''
}

process sankey_records {
    publishDir 'output'
    cpus 1
    memory '8 GB'
    label 'r'
    input: tuple path('*'), path(script)
    output: path("sankey_amounts.html") 
    shell: '''
Rscript -e "knitr::spin('!{script}')" 
'''
}



/*
//process analyze_observations_bartender {
//    cpus 16
//    memory '32 GB'
//    label 'r'
//    publishDir 'output'
//    input:
//        file("analyze_observations_bartender.R") from Channel.fromPath("scripts/analyze_observations.R")
//        tuple file(all_obs), file(counted_obs) from observations_bartender
//        file(sample_sheet) from Channel.fromPath('data/dme317_sample_codes.csv')
//        file(known_barcodes) from Channel.fromPath('data/known_barcodes.csv')
//    output:
//        file("analyze_observations_bartender.html") into report_bartender
//        file("glabrata_observed_codes*.csv") into observed_codes
//    shell:
//        '''
//        Rscript -e "knitr::spin('analyze_observations_bartender.R')" !{counted_obs} !{sample_sheet} !{known_barcodes}
//        '''
//}
*/

// Making directories 
[ './tmp', './output', './reports' ].each{file(it).mkdirs()}
